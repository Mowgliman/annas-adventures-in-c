#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main ( ) {
    time_t t;
	srand48((unsigned) time(&t));
    float turn = drand48();
    turn = turn * 100.0;

    if (turn < 32.0) {
        printf("Ja\n");
    }
    else if (turn < 64.0) {
        printf("Nein\n");
    }
    else if (turn < 96.0) {
        printf("Vielleicht\n");
    }
    else {
        printf("Saufen!!!\n");
    }
}