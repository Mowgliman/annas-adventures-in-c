#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define N 100

int boxes[N]; //Boxen sind nummeriert

int main ( ) {
    time_t t;
	srand48((unsigned) time(&t));
//Boxen Zettel zuweisen
    for (int i = 0; i<N; i++) {
        boxes[i] = rand( );
        printf ("%d\n", boxes[i]);
    }
}